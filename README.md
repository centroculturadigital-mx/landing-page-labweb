# Página para el Labweb
 
## Prueba
 
 
#### Instrucciones para participar
 
- Solicita acceso en el link debajo del nombre "Request Access"
- Clona el repo esta carpeta contiene
- Crea una rama con tu nombre
- Trabaja tu código
- Cuando esté lista tu prueba, haz push y crea una merge request
- Envía un correo a raquelglucio.ccd@gmail.com
 
#### Qué evaluaremos
 
- Creatividad
- Solución de problemas
- Código ordenado y fácil de leer
- Documentación legible

#### Recomendaciones

- Lee bien las instrucciones
- Trabaja una maqueta aunque sea un dibujo
- Revisa qué código es reusable para crear componentes y optimizar tus esfuerzos
- Crea un plan de tareas simples y calcula tu tiempo
- Ayúdate de tu editor de texto para facilitar cosas como el formato de tu código (puedes utilizar prettier por ejemplo)
- Busca que tu código sea descriptivo y ordenado
- Documenta tu proceso, cuéntanos qué compone la página y que hace cada parte. Imagina que la documentación es para tu yo del futuro :) 
 
### Objetivo
 
Crear una sitio pequeño con diseño libre para presentar al laboratorio web con **Reactjs**.
Landing page que comunique la personalidad del laboratorio y nos permita compartir tutoriales, repos colaborativos y artículos cortos.
 
### Requerimientos

- No requiere backend
- Sitio Responsivo
- Home : (debe contener)
   - Header creado como componente para contener el logo y link a la página del ccd.
   - Presentación del laboratorio
   - Grid para 4 publicaciones (texto de posición)
        - Card de publicación creado como componente para contener, imagen, título, fecha de publicación
   - Footer creado como componente para contener redes sociales del ccd y link a gitlab del Laboratorio web
  
    **Sería un plus si construyes para página de las publicación (home/slug-de-publicación)**

![Maqueta](./estructura.png)

 
### Sobre el Laboratorio web
 
El laboratorio web es un espacio multidisciplinario que participa en proyectos de índole cultural y artístico. Muchas veces hemos querido compartir experimentos o librerías, pero no tenemos una página como equipo. Nos gustaría que nos ayudaras a construir un front simple y flexible. El diseño es libre.

